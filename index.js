const express = require('express')// Include ExpressJS
const mongoose = require("mongoose")

mongoose.connect('mongodb://localhost:27017/test');

const User = mongoose.model('user', { username: String , password: String});


const app = express() // Create an ExpressJS app
const bodyParser = require('body-parser'); // Middleware

app.use(bodyParser.urlencoded({ extended: false }));

// Route to Homepage
app.get('/', (req, res) => {
  res.sendFile(__dirname + '/static/index.html');
});

// Route to Login Page
app.get('/login', (req, res) => {
  res.sendFile(__dirname + '/static/login.html');
});


// Route to Login Page
app.get('/register', (req, res) => {
    res.sendFile(__dirname + '/static/register.html');
  });
  
app.post('/login', async(req, res) => {
  // Insert Login Code Here
  let username = req.body.username;
  let password = req.body.password;
  const user = await User.findOne({username, password})
  
  if (user) {
    res.send(`Login successfully Username: ${username} Password: ${password}`);
  }
  else res.send(`User with data not found`);
 
});

  
app.post('/register', async (req, res) => {
    // Insert Login Code Here
    let username = req.body.username;
    let password = req.body.password;

    const user = await User.findOne({username})

    if (user) res.send(`${username} is registered`);
    else {
        await User.create({username, password})
        res.send(`Register successfully Username: ${username} Password: ${password}`);
    }
  
  });

const port = 3000 // Port we will listen on

// Function to listen on the port
app.listen(port, () => console.log(`This app is listening on port ${port}`));